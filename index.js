require('newrelic');
const app = require('express')();
const fs = require('fs');
const spdy = require('spdy');

let onSuccess = () => {
    const options = {
        key: fs.readFileSync(app.settings.sslKey, 'ascii'),
        cert:  fs.readFileSync(app.settings.sslCert, 'ascii')
    };

    spdy
        .createServer(options, app)
        .listen(app.settings.port, () => console.log(`Pantree API listening on port ${app.settings.port}\n`));
};

let onError = (error) => {
    console.log(error);
    return process.exit(1);
};

require('./settings')(app);
require('./databases')(app)
    .then(() => { require('./models')(app) })
    .then(() => { require('./middlewares')(app) })
    .then(() => { require('./actions')(app) })
    .then(() => { require('./routes')(app) })
    .then(onSuccess)
    .catch(onError);
