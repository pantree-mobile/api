module.exports = (app) => {
    console.log('Loading cache...');

    function get(req, res, next) {
        app.redis.get(req.originalUrl, (err, result) => {
            if (result) return res.send(JSON.parse(result));
            else next()
        });
    }

    return {
        get
    }
};
