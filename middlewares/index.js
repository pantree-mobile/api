module.exports = (app) => {
    console.log('Loading middlewares...');

    app.middlewares = {
        parsers: require('./parsers')(app),
        security: require('./security')(app),
        tools: require('./tools')(app),
        validators: require('./validators')(app)
    };
};
