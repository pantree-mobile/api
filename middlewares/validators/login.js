module.exports = (req , res, next) => {
    if (!req.body || !req.body.email || !req.body.password) {
        return res.status(400).send('Missing login fields (email, password)');
    }

    let emailRegExp = new RegExp(/^([\w_\.\-\+])+\@([\w\-]+\.)+([\w]{2,10})+$/);
    if (!emailRegExp.test(req.body.email)) {
        return res.status(400).send('Wrong email value. (xxx@xxx.xxx)');
    }

    if (req.body.password.length < 5) {
        return res.status(400).send('Wrong password value. (min length 5)');
    }

    return next();
};