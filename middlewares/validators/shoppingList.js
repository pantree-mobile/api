module.exports = (req , res, next) => {
    if (!req.body || !req.body.name) {
        return res.status(400).send('Missing shopping list fields (name, date?)');
    }

    return next();
};