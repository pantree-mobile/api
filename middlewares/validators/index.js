module.exports = (app) => {
    return {
        login: require('./login'),
        product: require('./product'),
        shoppingList: require('./shoppingList'),
        user: require('./user')
    }
};