module.exports = (req , res, next) => {

    if (!req.body || !req.body.count || !req.body.productId) {
        return res.status(400).send('Missing product add fields (productId, count)');
    }

    let countNumber = Number(req.body.count);
    if (isNaN(countNumber) || countNumber < 0) {
        return res.status(400).send('Wrong count number (must be >= 0)');
    }

    return next();
};