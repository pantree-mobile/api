const jwt = require('jsonwebtoken');

module.exports = (app) => {
    const User = app.models.User;

    return (req, res, next) => {
        if(!req.headers || !req.headers.authorization) return res.status(403).send('Authentication required');

        jwt.verify(req.headers.authorization, app.settings.security.salt, null, (err, decryptedToken) => {
            if (err) return res.status(401).send('Invalid token');

            app.redis.get(decryptedToken.userId, (err, token) => {
                if(err) return res.status(500).send(err);
                if(!token) return res.status(403).send('Invalid token');

                User.findOne({
                    where: { id: decryptedToken.userId },
                    attributes: { exclude: ['password'] }
                }).then((data) => {
                    if (!data) return res.status(403).send('No user attached to this token');
                    req.user = data;
                    next();
                }).catch((err) => {
                    res.status(500).send(err.message);
                });
            });
        });
    }

};
