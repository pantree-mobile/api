module.exports = (app) => {
    return {
        findByEmailPassword: require('./findByEmailPassword')(app)
    };
};