const sha1 = require('sha1');

module.exports = (app) => {
    const User = app.models.User;

    return function findByEmailPassword(req, res, next) {
        User.findOne({
            where: {email: req.body.email, password: sha1(req.body.password)}
        }).then((user) => {
            req.user = user;
            next();
        }).catch((err) => {
            return res.status(500).send(err.message);
        });
    }
};
