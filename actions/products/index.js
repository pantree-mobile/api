module.exports = (app) => {
    return {
        findOneByCode: require('./findOneByCode')(app),
        upsert: require('./upsert')(app)
    };
};