module.exports = (app) => {
    const Product = app.models.Product;
    const RawProduct = app.models.RawProduct;

    return function upsert(req, res, next) {
        if (!req.product) {
            RawProduct.findOne({code: req.body.productId}, (err, rawProduct) => {
                if (err) return res.status(500).send(err);
                if (!rawProduct) return res.status(404).send("This product doesn't exist in our database");

                let splitCode = rawProduct.code.match(/.{3}/g).join('/');
                rawProduct.keywords = rawProduct._keywords.join(',');
                rawProduct.image_url = app.settings.imagesBaseUrl + splitCode + rawProduct.code[rawProduct.code.length - 1] + "/1.400.jpg";
                rawProduct.image_small_url = app.settings.imagesBaseUrl + splitCode + rawProduct.code[rawProduct.code.length - 1] + "/1.100.jpg";

                Product.build(rawProduct).save().then((product) => {
                    req.product = product;
                    next();
                }).catch((err) => {
                    res.status(500).send(err);
                });
            });
        } else {
            next();
        }
    }
};
