module.exports = (app) => {
    return {
        update: require('./update')(app),
        findAll: require('./findAll')(app),
        findOne: require('./findOne')(app),
    };
};