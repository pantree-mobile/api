module.exports = () => {
    return function findOne(req, res) {
        req.user.getProducts({
            where: {code: req.params.productId}
        }).then((products) => {
            if (products.length === 0) return res.status(404).send('Not found');
            res.send(products[0]);
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};
