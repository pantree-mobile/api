module.exports = () => {
    return function update(req, res) {
        req.user.getProducts({
            where: { code: req.body.productId}
        }).then((products) => {
            if (products.length === 0) {
                req.user.addProduct(req.product, { through: { count: Number.parseInt(req.body.count) }}).then(() => {
                    res.sendStatus(req.product);
                }).catch((err) => {
                    res.status(500).send(err);
                });
            } else {
                products[0].userproduct.count = Number.parseInt(req.body.count);
                products[0].userproduct.save().then(() => {
                    res.sendStatus(products[0]);
                }).catch((err) => {
                    res.status(500).send(err);
                });
            }
        }).catch((err) => res.status(500).send(err));
    }
};