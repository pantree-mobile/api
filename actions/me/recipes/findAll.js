module.exports = () => {

    return function findAll(req, res) {
        req.user.getRecipes().then((recipes) => {
            res.send(recipes);
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};