const request = require('request');
const Op = require('sequelize').Op;

module.exports = (app) => {
    return function findSuggestions(req, res) {
        req.user.getProducts({
            through: { where: { count: { [Op.gt]: 0 } } }
        }).then((products) => {
            let keywords = "";
            let url = app.settings.recipesBaseUrl;
            products.forEach((product) => keywords += product.keywords.split(',').slice(1).join(',') + ',');
            url += "?i=chicken,tomato,pepper";
            console.log("request:", url);

            request(url, { json: true, timeout: 15000}, (err, response, body) => {
                if (err) {console.log(err); return res.status(500).send(err);}
                res.send(body.results);
            });
        }).catch((err) => {
            console.log(err);
            res.status(500).send(err);
        });
    }
};