module.exports = () => {
    return function findOne(req, res) {
        req.user.getRecipes({
            where: {id: req.params.recipeId}
        }).then((recipe) => {
            if (recipe.length === 0) return res.status(404).send('Not found');
            res.send(recipe[0]);
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};