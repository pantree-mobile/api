const Op = require('sequelize').Op;

module.exports = (app) => {
    return function findAll(req, res, next) {
        req.user.getShoppinglists({
            include: [ { model: app.models.Product, as: 'products', through: { where: { count: { [Op.gt]: 0 } } } }  ]
        }).then((shoppingLists) => {
            res.send(shoppingLists);
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};
