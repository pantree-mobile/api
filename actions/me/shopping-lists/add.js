module.exports = (app) => {
    const ShoppingList = app.models.ShoppingList;

    return function add(req, res) {
        req.body.userId = req.user.id;

        ShoppingList.build(req.body).save().then((shoppingList) => {
            res.send(shoppingList);
        }).catch((err) => {
            res.status(500).send(err);
        })
    }
};