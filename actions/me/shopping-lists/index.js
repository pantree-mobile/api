module.exports = (app) => {
    return {
        add: require('./add')(app),
        findAll: require('./findAll')(app),
        findOne: require('./findOne')(app),
        findProductsSuggestions: require('./findProductsSuggestions')(app),
        remove: require('./remove')(app),
        sendOne: require('./sendOne')(app),
        update: require('./update')(app),
        products: require('./products/index')(app)
    };
};