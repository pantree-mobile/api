module.exports = (app) => {
    return {
        buy: require('./buy')(app),
        findOne: require('./findOne')(app),
        update: require('./update')(app)
    };
};