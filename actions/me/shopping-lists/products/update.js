module.exports = () => {
    return function update(req, res) {
        req.shoppingList.addProduct(req.product, { through: { count: Number.parseInt(req.body.count) }}).then(() => {
            res.sendStatus(201);
        }).catch((err) => {
            res.status.send(err);
        });
    }
};