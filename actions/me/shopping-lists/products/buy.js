module.exports = () => {
    return function buy(req, res) {
        req.user.getProducts({
            where: { code: req.product.code}
        }).then((products) => {
            if (products.length === 0) {
                req.user.addProduct(req.product, { through: { count: Number.parseInt(req.product.shoppinglistproduct.count) }}).then(() => {
                    req.shoppingList.removeProduct(req.product).then(() => {
                        res.sendStatus(201);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }).catch((err) => {
                    res.status(500).send(err);
                });
            } else {
                products[0].userproduct.count += Number.parseInt(req.product.shoppinglistproduct.count);
                products[0].userproduct.save().then(() => {
                    req.shoppingList.removeProduct(req.product).then(() => {
                        res.sendStatus(201);
                    }).catch((err) => {
                        res.status(500).send(err);
                    });
                }).catch((err) => {
                    res.status(500).send(err);
                });
            }
        }).catch((err) => res.status(500).send(err));
    }
};