module.exports = () => {
    return function findOne(req, res, next) {
        req.shoppingList.getProducts({
            where: { code: req.params.productId}
        }).then((products) => {
            if (products.length === 0) return res.status(404).send('Product not in shopping list');
            req.product = products[0];
            next();
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};
