const Op = require('sequelize').Op;

module.exports = (app) => {
    return function findOne(req, res, next) {
        req.user.getShoppinglists({
            include: [ { model: app.models.Product, as: 'products', through: { where: { count: { [Op.gt]: 0 } } } }  ],
            where: {id: req.params.shoppingListId}
        }).then((shoppingLists) => {
            if (shoppingLists.length === 0) return res.status(404).send('Not found');
            req.shoppingList = shoppingLists[0];
            next();
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};
