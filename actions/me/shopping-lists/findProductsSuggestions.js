const Op = require('sequelize').Op;

module.exports = () => {
    return function findProductsSuggestions(req, res) {
        req.user.getProducts({ through: { where: { count: { [Op.eq]: 0 } } }}).then((products) => {
            res.send(products);
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};
