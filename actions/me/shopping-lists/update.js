module.exports = () => {
    return function update(req, res) {
        req.user.getShoppinglists({ where: { id: req.params.shoppingListId } }).then((shoppingLists) => {
            if (!shoppingLists.length) return res.status(404).send('Shopping list not found');
            shoppingLists[0].updateAttributes(req.body);
            res.send(shoppingLists[0]);
        }).catch((err) => {
            res.status(500).send(err.message);
        });
    }
};