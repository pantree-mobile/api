module.exports = () => {
    return function remove(req, res) {
        req.user.getShoppinglists({
            where: {id: req.params.shoppingListId}
        }).then((shoppingLists) => {
            if (shoppingLists.length === 0) return res.status(404).send('Not found');

            shoppingLists[0].destroy().then(() => {
                res.sendStatus(204);
            }).catch((err) => {
                res.status(500).send(err);
            });
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};