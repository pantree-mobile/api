module.exports = (app) => {
    return {
        findOne: require('./findOne')(app),
        products: require('./products')(app),
        recipes: require('./recipes')(app),
        shoppingLists: require('./shopping-lists')(app)
    };
};