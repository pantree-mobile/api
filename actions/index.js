module.exports = (app) => {
    console.log('Loading actions...');

    app.actions = {
        authentication: require('./authentication')(app),
        me: require('./me')(app),
        products: require('./products')(app),
        rawProducts: require('./raw-products')(app),
        users: require('./users')(app)
    };
};