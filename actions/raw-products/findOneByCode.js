module.exports = (app) => {
    const Product = app.models.Product;

    return function findOneByCode(req, res, next) {
        Product.findOne({
            where: { code: req.params.productId }
        }).then((product) => {
            req.product = product;
            next();
        }).catch((err) => {
            res.status(500).send(err);
        });
    }
};
