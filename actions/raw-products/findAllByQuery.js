module.exports = (app) => {
    const RawProduct = app.models.RawProduct;

    return function findAllByQuery(req, res, next) {
        RawProduct
            .find({
                $or: [
                    { product_name: { $regex: req.query.query, $options: 'i' } },
                    { categories: { $regex: req.query.query, $options: 'i' } }
                ]
            })
            .select('code product_name categories')
            .limit(10)
            .exec((error, products) => {
                if (error) return res.status(500).send(error);
                app.redis.setex(req.originalUrl, 3600, JSON.stringify(products));
                return res.send(products);
            });
    }
};
