module.exports = (app) => {
    return {
        findOneByCode: require('./findOneByCode')(app),
        findAllByQuery: require('./findAllByQuery')(app)
    };
};