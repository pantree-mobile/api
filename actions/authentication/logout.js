module.exports = (app) => {

    return function logout(req, res) {
        app.redis.del(req.user.id, function(err) {
            if (err) return res.status(500).send(err);
            res.sendStatus(204);
        });
    }
};
