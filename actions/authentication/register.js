const sha1 = require('sha1');

module.exports = (app) => {
    const User = app.models.User;

    return function register(req, res, next) {
        req.body.encryptedPassword = sha1(req.body.password);

        User.findOrCreate({
            where: { email: req.body.email },
            defaults: { password: req.body.encryptedPassword, name: req.body.name }
        }).spread((user, created) => {
            if (created) next();
            else return res.status(401).send('Email already in use.');
        });
    }
};