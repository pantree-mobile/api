const jwt = require('jsonwebtoken');

module.exports = (app) => {
    return function login(req, res) {
        if(!req.user) return res.status(401).send('Invalid credentials');

        jwt.sign({ userId : req.user.id }, app.settings.security.salt, { expiresIn: '1d' }, (err, token) => {
            if(err) return res.status(500).send(err);

            app.redis.set(req.user.id, token, (err) => {
                if (err) return res.status(500).send(err);
                res.send({ access_token: token, expires_at: Math.trunc(Date.now() / 1000) + 86400})
            });
        });
    }
};
