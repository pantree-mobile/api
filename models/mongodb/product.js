const Schema = require('mongoose').Schema;

module.exports = (app) => {
    const schema = new Schema({
        code: {
            type: String
        },
        product_name: {
            type: String
        },
        categories: {
            type: String
        },
        image_small_url: {
            type: String
        },
        _keywords: {
            type: Array
        },
        product_quantity: {
            type: Number
        },
        quantity: {
            type: String
        }
    });

    return app.mongoose.model('Product', schema);
};