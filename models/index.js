module.exports = (app) => {
    console.log('Loading models...');

    app.models = {
        Product: require('./mysql/product')(app),
        Recipe : require('./mysql/recipe')(app),
        ShoppingList : require('./mysql/shoppinglist')(app),
        ShoppingListProduct : require('./mysql/shoppinglistproduct')(app),
        User : require('./mysql/user')(app),
        UserProduct : require('./mysql/userproduct')(app),
        RawProduct: require('./mongodb/product')(app)
    };

    app.models.User.hasMany(app.models.ShoppingList);
    app.models.User.belongsToMany(app.models.Recipe, { through: 'UserRecipe', as: 'recipes' });
    app.models.User.belongsToMany(app.models.Product, { through: app.models.UserProduct, as: 'products' });

    app.models.Recipe.belongsToMany(app.models.Product, { through: 'RecipeProduct', as: 'products' });

    app.models.ShoppingList.belongsToMany(app.models.Product, { through: app.models.ShoppingListProduct, as: 'products' });
    app.models.ShoppingList.belongsTo(app.models.User);

    app.sequelize.sync();
};
