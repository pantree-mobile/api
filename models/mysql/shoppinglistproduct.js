const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('shoppinglistproduct', {
        count: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        }
    });
};
