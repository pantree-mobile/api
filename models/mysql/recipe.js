const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('recipe', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        title: {
            type: Sequelize.STRING
        },
        href: {
            type: Sequelize.STRING
        },
        ingredients: {
            type: Sequelize.TEXT
        },
        thumbnail: {
            type: Sequelize.STRING
        }
    });
};
