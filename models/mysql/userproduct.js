const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('userproduct', {
        count: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        }
    });
};
