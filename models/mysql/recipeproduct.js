const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('recipeproduct', {
        count: {
            type: Sequelize.INTEGER
        }
    });
};
