const Sequelize = require('sequelize');

module.exports = (app) => {
    return app.sequelize.define('product', {
        code: {
            type: Sequelize.STRING,
            primaryKey: true,
            allowNull: false
        },
        product_name: {
            type: Sequelize.STRING
        },
        categories: {
            type: Sequelize.TEXT
        },
        image_url: {
            type: Sequelize.STRING
        },
        image_small_url: {
            type: Sequelize.STRING
        },
        product_quantity: {
            type: Sequelize.DECIMAL(10, 2)
        },
        quantity: {
            type: Sequelize.STRING
        },
        keywords: {
            type: Sequelize.TEXT
        }
    });
};
