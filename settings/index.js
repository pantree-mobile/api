module.exports = (app) => {
    console.log('\nLoading settings...');

    return new Promise((resolve) => {
        app.settings = require('./settings.json');
        resolve();
    });
};