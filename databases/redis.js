const redis = require('redis');

module.exports = (app, resolve, reject) => {
    // const client = redis.createClient({ port: app.settings.redis.port, host: app.settings.redis.host, password: app.settings.redis.password});
    const client = redis.createClient({ port: app.settings.redis.port, host: app.settings.redis.host});

    let onConnect = () => {
        app.redis = client;
        console.log(`Redis connected successfully`);
        resolve();
    };

    let onError = (error) => {
        console.log(`Redis ERROR: ${error}`);
        reject(`[ERROR: ${error.code}] Cannot connect to Redis`);
    };

    client.on('connect', onConnect);
    client.on('error', onError);
};