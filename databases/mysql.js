const Sequelize = require('sequelize');

module.exports = (app, resolve, reject) => {
    const url = `mysql://${app.settings.mysql.user}:${app.settings.mysql.password}@${app.settings.mysql.host}:${app.settings.mysql.port}/${app.settings.mysql.database}`;

    let onSuccess = () => {
        console.log(`Sequelize connected successfully`);
        resolve();
    };

    let onError = (error) => {
        console.log('MySQL ', error);
        reject(`[ERROR: ${error.code}] Cannot connect to MySQL`);
    };

    app.sequelize = new Sequelize(app.settings.mysql.database, app.settings.mysql.user, app.settings.mysql.password, { host: app.settings.mysql.host, dialect: 'mysql', logging: false, operatorsAliases: Sequelize.Op});
    app.sequelize.authenticate()
        .then(onSuccess)
        .catch(onError);
};
