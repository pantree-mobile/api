module.exports = (app) => {
    console.log('Loading databases...');

    let mongoInit = new Promise((resolve, reject) => {
        require('./mongodb')(app, resolve, reject);
    });

    let mysqlInit = new Promise((resolve, reject) => {
        require('./mysql')(app, resolve, reject);
    });

    let redisInit = new Promise((resolve, reject) => {
        require('./redis')(app, resolve, reject);
    });

    return Promise.all([mongoInit, mysqlInit, redisInit]);
};