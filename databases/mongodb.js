const mongoose = require('mongoose');

module.exports = (app, resolve, reject) => {
    const url = `mongodb://${app.settings.mongodb.user}:${app.settings.mongodb.password}@${app.settings.mongodb.host}:${app.settings.mongodb.port}/${app.settings.mongodb.database}`;

    let onSuccess = () => {
        app.mongoose = mongoose;
        console.log(`Mongoose connected successfully`);
        resolve();
    };

    let onError = (error) => {
        console.log(`MongoDB ERROR: ${error}`);
        reject(`[ERROR: ${error.code}] Cannot connect to MongoDB`);
    };

    mongoose.connect(url, { useNewUrlParser: true })
        .then(onSuccess)
        .catch(onError);
};