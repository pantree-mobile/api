const express = require('express');
const router = express.Router({});

module.exports = (app) => {

    router.use(
        app.middlewares.security.isAuthenticated
    );

    // ME
    router.get('/',
        app.actions.me.findOne
    );

    // PRODUCTS //
    router.get('/products',
        app.actions.me.products.findAll
    );

    router.post('/products',
        app.middlewares.parsers.bodyparser.json(),
        app.middlewares.validators.product,
        app.actions.products.findOneByCode,
        app.actions.products.upsert,
        app.actions.me.products.update
    );

    router.get('/products/:productId',
        app.actions.me.products.findOne
    );

    // RECIPES //
    router.get('/recipes',
        app.actions.me.recipes.findAll
    );

    router.get('/recipes/suggestions',
        app.actions.me.recipes.findSuggestions
    );

    router.get('/recipes/:recipeId',
        app.actions.me.recipes.findOne
    );

    // SHOPPING LISTS //
    router.post('/shopping-lists',
        app.middlewares.parsers.bodyparser.json(),
        app.middlewares.validators.shoppingList,
        app.actions.me.shoppingLists.add
    );

    router.get('/shopping-lists',
        app.actions.me.shoppingLists.findAll
    );

    router.get('/shopping-lists/suggestions',
        app.actions.me.shoppingLists.findProductsSuggestions
    );

    router.get('/shopping-lists/:shoppingListId',
        app.actions.me.shoppingLists.findOne,
        app.actions.me.shoppingLists.sendOne
    );

    router.delete('/shopping-lists/:shoppingListId',
        app.actions.me.shoppingLists.remove
    );

    router.post('/shopping-lists/:shoppingListId/products',
        app.middlewares.parsers.bodyparser.json(),
        app.middlewares.validators.product,
        app.actions.products.findOneByCode,
        app.actions.products.upsert,
        app.actions.me.shoppingLists.findOne,
        app.actions.me.shoppingLists.products.update
    );

    router.post('/shopping-lists/:shoppingListId/products/:productId/buy',
        app.middlewares.parsers.bodyparser.json(),
        app.actions.me.shoppingLists.findOne,
        app.actions.me.shoppingLists.products.findOne,
        app.actions.me.shoppingLists.products.buy
    );

    return router;
};
