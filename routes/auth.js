const router = require('express').Router({});

module.exports = (app) => {
    router.post('/login',
        app.middlewares.parsers.bodyparser.json(),
        app.middlewares.validators.login,
        app.actions.users.findByEmailPassword,
        app.actions.authentication.login
    );

    router.post('/logout',
        app.middlewares.security.isAuthenticated,
        app.actions.authentication.logout
    );

    router.post('/register',
        app.middlewares.parsers.bodyparser.json(),
        app.middlewares.validators.login,
        app.actions.authentication.register,
        app.actions.users.findByEmailPassword,
        app.actions.authentication.login
    );
    
    return router;
};
