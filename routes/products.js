const router = require('express').Router({});

module.exports = (app) => {
    router.get('/',
        app.middlewares.tools.cache.get,
        app.actions.rawProducts.findAllByQuery
    );
    
    return router;
};
