const cors = require('cors');
const compression = require('compression');

module.exports = (app) => {
    console.log('Loading routes...\n');

    app.use(compression({threshold:0}));
    app.use(cors());
    app.use('/auth', require('./auth')(app));
    app.use('/me', require('./me')(app));
    app.use('/products', require('./products')(app));
};
